# Koillection Cloudron App

This repository contains the Cloudron app package source for [Koillection](https://github.com/benjaminjonard/koillection).

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd koillection-app

cloudron build
cloudron install
```
