FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

RUN apt-get remove -y php-* php8.1-* libapache2-mod-php8.1 && \
    apt-get autoremove -y && \
    add-apt-repository --yes ppa:ondrej/php && \
    apt update && \
    apt install -y php8.2 php8.2-{apcu,bcmath,bz2,cgi,cli,common,curl,dba,dev,enchant,fpm,gd,gmp,gnupg,imagick,imap,interbase,intl,ldap,mailparse,mbstring,mysql,odbc,opcache,pgsql,phpdbg,pspell,readline,redis,snmp,soap,sqlite3,sybase,tidy,uuid,xml,xmlrpc,xsl,zip,zmq} libapache2-mod-php8.2 && \
    apt install -y php-{date,pear,twig,validate} && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# https://getcomposer.org/download/
RUN curl --fail -L https://getcomposer.org/download/2.2.7/composer.phar -o /usr/bin/composer && chmod +x /usr/bin/composer

# this binaries are not updated with PHP_VERSION since it's a lot of work
RUN update-alternatives --set php /usr/bin/php8.2 && \
    update-alternatives --set phar /usr/bin/phar8.2 && \
    update-alternatives --set phar.phar /usr/bin/phar.phar8.2 && \
    update-alternatives --set phpize /usr/bin/phpize8.2 && \
    update-alternatives --set php-config /usr/bin/php-config8.2

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN a2disconf other-vhosts-access-log && a2dismod perl && a2enmod rewrite env remoteip

ARG VERSION=1.4.13

RUN curl -LSs https://github.com/benjaminjonard/koillection/archive/${VERSION}.tar.gz | tar -xz -C /app/code/ --strip-components 1 -f -

RUN chown www-data:www-data -R /app/code

RUN sudo -u www-data composer install --no-interaction --classmap-authoritative --no-dev

RUN rm -f /app/code/.env && ln -sf /app/data/env /app/code/.env

COPY env-init.production /app/data/env

RUN php bin/console app:translations:dump

RUN php bin/console lexik:jwt:generate-keypair

RUN cd assets/ && yarn install && yarn build

RUN rm -rf /app/code/var && \
    ln -s /app/data/var /app/code/var

RUN mkdir -p /app/data/var/session /run/php && ln -s /app/data/var/session /run/php/session

# configure apache
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf
COPY apache/koillection.conf /etc/apache2/sites-enabled/koillection.conf
RUN echo "Listen 80" > /etc/apache2/ports.conf
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

RUN a2disconf other-vhosts-access-log
# disable perl for php locale to work
RUN a2enmod rewrite php8.2 && a2dismod perl

RUN crudini --set /etc/php/8.2/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/8.2/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/8.2/apache2/php.ini PHP memory_limit 64M && \
    crudini --set /etc/php/8.2/apache2/php.ini Session session.save_path /run/php/session && \
    crudini --set /etc/php/8.2/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.2/apache2/php.ini Session session.gc_divisor 100

RUN ln -s /app/data/php.ini /etc/php/8.2/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.2/cli/conf.d/99-cloudron.ini

WORKDIR /app/code
ADD start.sh env.production /app/pkg/

RUN chown -R www-data.www-data /app/code

CMD [ "/app/pkg/start.sh" ]
